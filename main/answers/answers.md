#JAG Method software developer assessment
## Answers

### 1. SEO (5min)

* We can add keywords to the meta data as well as a description. Something like:

<meta name="keywords" content="JAG, provides, best, insurance, qoutes" />
OR
<meta name="description" content="Request a qoutes to get JAG's best offers" />

* We can create a robots.txt and a sitemap.xml file 

* Ensure that only 1 <h1></h1> tag is used on each page as this tells us what the page is all about

* Ensure that the page is tracked by google analytics so that you can monitor the statistics.

### 2. Responsive (15m)

* Rendering the screen on a  mobile or less than 480px causes the JAG Method logo to display off the screen. In order to fix this I added the below code to the JAG.css file:

@media (max-width: 480px) {
    #hypLogo {
    padding-left: 20px;
    }
}

* For the car insurance banner at the top there is not much we can do to this overlap issue where it does not show the ladies face. This comes down to when the writing overlaps onto the image then it is not really readible so making that split 50% will not be worth while. I would suggest removing the image after a specific width
* The menu on a mobile screen does not render so nicely. I would definelty add some padding so taht that it is right aligned in the menu so that there is a space between the end of the wording and the right side of the screen.#
* Due to the time limit on this question I was not able to progress further. Bootstrap is not one of my strong points however given some time I would be able to make a few improvements. 


### 10. Data Analysis (30m)

1. Total Profit
**Answer**
-77123.99201263015971

**SQL**
SELECT
	  SUM([Earnings] - [Cost])
  FROM [JAGTest].[dbo].[LeadDetail]

2. Total Profit (Earnings less VAT)
**Answer**
-89681.5013739304050990

**SQL**
SELECT 
      SUM( ([Earnings] - ([Earnings] * 0.15)) - [Cost])
  FROM [JAGTest].[dbo].[LeadDetail]

3. Loss making campaigns
**Answer**
Campaign 111
Campaign 117
Campaign 13
Campaign 130
Campaign 137
Campaign 14
Campaign 140
Campaign 141
Campaign 15
Campaign 159
Campaign 16
Campaign 161
Campaign 167
Campaign 168
Campaign 169
Campaign 178
Campaign 182
Campaign 184
Campaign 185
Campaign 187
Campaign 189
Campaign 3
Campaign 63
Campaign 7
Campaign 76
Campaign 77
Campaign 79
Campaign 8
Campaign 87
Campaign 89
Campaign 90
Campaign 92
Campaign 94

**SQL**
SELECT 
cn.CampaignName,
SUM ([Earnings]),
SUM ([Cost])
  FROM [JAGTest].[dbo].[LeadDetail] as ld
  inner join Campaign as cn on ld.CampaignId = cn.CampaignId
  GROUP BY cn.CampaignName


4. Average conversion rate
**Answer**
0

**SQL**
		  SELECT AVG(X.ConversionRate) FROM (SELECT 
	  [LeadId],
	  (SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.LeadId = ld.LeadId and ldd.IsSold = 1) as Sold,
	  (SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.LeadId = ld.LeadId and ldd.IsAccepted = 1) as Accepted,
	  ((SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.LeadId = ld.LeadId and ldd.IsSold = 1) / NULLIF((SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.LeadId = ld.LeadId and ldd.IsAccepted = 1), 0)) as 'ConversionRate'
  FROM [JAGTest].[dbo].[LeadDetail] as ld
  	GROUP BY [LeadId]) X

5. Conversion rate more than 10% for 2 months
**Answer**

I would go with Client 16. Client 16 has many accepted orders and generates the top 2nd profit for the company. 
For the second client I would go with Client 182. Although he does not place many orders his order profits are quite large which is a positive for the company

**SQL**
Query A:

SELECT 
cn.ClientName,
ldl.ClientId,
SUM (([Earnings] - ([Earnings] * 0.15)) - [Cost]) as Profit
  FROM [JAGTest].[dbo].[LeadDetail] as ldl
  inner join Client as cn on ldl.ClientId = cn.ClientId
  GROUP BY cn.ClientName, ldl.ClientId
  ORDER By Profit DESC

  Query B:

  SELECT 
  cn.ClientName,
  ld.ClientId,
	  (SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.ClientId = ld.ClientId and ldd.IsSold = 1) as Sold,
	  (SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.ClientId = ld.ClientId and ldd.IsAccepted = 1) as Accepted,
	  ((SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.ClientId = ld.ClientId and ldd.IsSold = 1) / NULLIF((SELECT COUNT(*) FROM [LeadDetail] as ldd where ldd.ClientId = ld.ClientId and ldd.IsAccepted = 1), 0)) as 'ConversionRate'
  FROM [JAGTest].[dbo].[LeadDetail] as ld
    inner join Client as cn on ld.ClientId = cn.ClientId
  	GROUP BY cn.ClientName, ld.ClientId