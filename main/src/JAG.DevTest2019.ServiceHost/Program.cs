﻿using JAG.DevTest2019.ServiceHost.WebAPI;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Http;

namespace JAG.DevTest2019.ServiceHost
{
    class Program
    {
        private static IDisposable _WebApiServiceHost;
        private static System.Timers.Timer leadTimer;
        static int currentLeadRecords = 0;

        static void Main(string[] args)
        {
            Console.WriteLine($"Starting WebAPI");
            int apiPort = 49392;
            string apiHost = "http://localhost";
            string url = $"{apiHost}:{apiPort}";

            _WebApiServiceHost = WebApp.Start<WebApiStartup>(url);
            Console.WriteLine($"WebAPI hosted on {url}");

            leadTimer = new System.Timers.Timer(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewLeadPollingTime"] ?? "60000"));
            leadTimer.Elapsed += PollLeadResults;
            leadTimer.AutoReset = true;
            leadTimer.Enabled = true;
            leadTimer.Start();

            Console.WriteLine($"Press enter to exit");
            Console.ReadLine();
            leadTimer.Stop();
        }

        private static void PollLeadResults(Object source, ElapsedEventArgs e)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand leadCommand = new SqlCommand("SELECT COUNT(*) FROM [Lead]", connection);               
                    var leadExists = leadCommand.ExecuteScalar();

                    if (currentLeadRecords == 0)
                    {
                        currentLeadRecords = (int)leadExists;
                    }

                    var newLeadRecords = (int)leadExists - currentLeadRecords;

                    Console.WriteLine(newLeadRecords.ToString() + " new lead records");
                    currentLeadRecords = (int)leadExists;
                }
                catch
                {
                    //Log errors to audit log if required
                    Console.WriteLine("An error has occured. Failed to get new lead record count.");
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
