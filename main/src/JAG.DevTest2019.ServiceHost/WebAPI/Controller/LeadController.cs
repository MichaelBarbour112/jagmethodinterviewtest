﻿using JAG.DevTest2019.Model;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        [HttpPost]
        [ResponseType(typeof(LeadResponse))]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse()
            {
                IsCapped = false,
                Messages = new[] {"Success from WebAPI"},
                IsSuccessful = true,
                IsDuplicate = false
            };

            //TODO: 7. Write the lead to the DB
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var messagePosted = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    var leadExists = CheckIfLeadExists(request.EmailAddress, request.ContactNumber, connection);

                    if (leadExists)
                    {
                        response.IsDuplicate = true;
                        response.IsSuccessful = false;
                        response.Messages = new[] {"duplicate on [" + request.EmailAddress + " / " + request.ContactNumber + "]"};

                        Console.WriteLine($"Lead received {request.FirstName} {request.Surname}. " + response.Messages[0]);
                        messagePosted = true;
                    }

                    var trackingCodeExceeded = CheckIfTrackingCodeExceedsLimit(request.TrackingCode, connection);
                    int trackingCodeLimit = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingCode"] ?? "10");

                    if (trackingCodeExceeded > trackingCodeLimit)
                    {
                        response.IsCapped = true;
                        response.IsSuccessful = false;
                        response.Messages = new[]
                            {"[" + trackingCodeExceeded + "] lead recieved today, code [" + request.TrackingCode + "] is capped"};

                        Console.WriteLine($"Lead received {request.FirstName} {request.Surname}. " + response.Messages[0]);
                        messagePosted = true;
                    }

                    var leadId = SaveLeadData(request, response, connection);
                    response.LeadId = leadId;

                }
                catch(Exception ex)
                {
                    //Catch Error and log it
                }
                finally
                {
                    connection.Close();
                }
            }

            if (!messagePosted)
            {
                Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");
            }

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        private bool CheckIfLeadExists(string email, string contactNumber, SqlConnection connection)
        {
            var leadCommand = "";
            if (string.IsNullOrEmpty(email))
            {
                leadCommand = "SELECT COUNT(*) FROM [LEAD] WHERE ContactNumber = @ContactNumber";
            }
            else
            {
                leadCommand =
                    "SELECT COUNT(*) FROM [Lead] AS L1 INNER JOIN [Lead] AS L2 ON L1.ContactNumber <> L2.ContactNumber AND L1.Email = @Email OR L2.ContactNumber = @ContactNumer ";
            }

            SqlCommand leadCheckCommand = new SqlCommand(leadCommand, connection);
            leadCheckCommand.Parameters.AddWithValue("@Email", email);
            leadCheckCommand.Parameters.AddWithValue("@ContactNumer", contactNumber);

            var leadExists = leadCheckCommand.ExecuteScalar();

            if (leadExists != null && (int)leadExists > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int CheckIfTrackingCodeExceedsLimit(string trackingCode, SqlConnection connection)
        {
            SqlCommand leadCommand = new SqlCommand("SELECT COUNT(*) FROM [Lead] WHERE TrackingCode = @TrackingCode", connection);
            leadCommand.Parameters.AddWithValue("@TrackingCode", trackingCode ?? "");
            var leadExists = leadCommand.ExecuteScalar();

            return (int)leadExists;
        }

        private int SaveLeadData(Lead request, LeadResponse response, SqlConnection connection)
        {
            int leadId = 0;

            var leadCommand =
                "INSERT INTO Lead (TrackingCode, FirstName, LastName, ContactNumber, Email, ReceivedDateTime, IPAddress, UserAgent, ReferrerURL, IsDuplicate, IsCapped, IsSuccessful, UserSessionId) " +
                " VALUES (@TrackingCode, @FirstName, @LastName, @ContactNumber, @Email, @ReceivedDateTime, @IPAddress, @UserAgent, @ReferrerURL, @IsDuplicate, @IsCapped, @IsSuccessful, @UserSessionId); SELECT CAST(scope_identity() AS int)";

            using (var insertLeadCommand = new SqlCommand(leadCommand, connection))
            {
                insertLeadCommand.Parameters.AddWithValue("@TrackingCode", request.TrackingCode ?? "");
                insertLeadCommand.Parameters.AddWithValue("@FirstName", request.FirstName);
                insertLeadCommand.Parameters.AddWithValue("@LastName", request.Surname);
                insertLeadCommand.Parameters.AddWithValue("@ContactNumber", request.ContactNumber);
                insertLeadCommand.Parameters.AddWithValue("@Email", request.EmailAddress);
                insertLeadCommand.Parameters.AddWithValue("@ReceivedDateTime", DateTime.Now.ToString());
                insertLeadCommand.Parameters.AddWithValue("@IPAddress", request.ClientIpAddress);
                insertLeadCommand.Parameters.AddWithValue("@UserAgent", request.UserAgent);
                insertLeadCommand.Parameters.AddWithValue("@ReferrerURL", request.ReferrerUrl);
                insertLeadCommand.Parameters.AddWithValue("@IsDuplicate", response.IsDuplicate);
                insertLeadCommand.Parameters.AddWithValue("@IsCapped", response.IsCapped);
                insertLeadCommand.Parameters.AddWithValue("@IsSuccessful", response.IsSuccessful);
                insertLeadCommand.Parameters.AddWithValue("@UserSessionId", request.UserSessionId);

                leadId = (int) insertLeadCommand.ExecuteScalar();
            }

            var leadParametersDataTable = CreateLeadParameterDataTable(request.LeadParameters, leadId);

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
            {
                bulkCopy.DestinationTableName = "dbo.LeadParameter";

                try
                {
                    bulkCopy.WriteToServer(leadParametersDataTable, DataRowState.Unchanged);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return leadId;
        }

        /// <summary>
        /// We can look at making this a generic method to accept any class structure.
        /// </summary>
        /// <returns></returns>
        private static DataTable CreateLeadParameterDataTable(LeadParameter[] leadParameters, int leadId)
        {
            DataTable leadParameter = new DataTable("LeadParameter");

            DataColumn leadParameterIdColumn = new DataColumn()
            {
                DataType = System.Type.GetType("System.Int32"),
                ColumnName = "LeadParameterId"
            };
            leadParameter.Columns.Add(leadParameterIdColumn);

            DataColumn leadIdColumn = new DataColumn()
            {
                DataType = System.Type.GetType("System.Int32"),
                ColumnName = "LeadId",
            };
            leadParameter.Columns.Add(leadIdColumn);

            DataColumn nameColumn = new DataColumn()
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "Name",
            };
            leadParameter.Columns.Add(nameColumn);

            DataColumn valueColumn = new DataColumn()
            {
                DataType = System.Type.GetType("System.String"),
                ColumnName = "Value",
            };
            leadParameter.Columns.Add(valueColumn);

            foreach (var parameter in leadParameters)
            {
                DataRow row = leadParameter.NewRow();
                row["LeadId"] = leadId;
                row["Name"] = parameter.Key;
                row["Value"] = parameter.Value;
                leadParameter.Rows.Add(row);
            }

            leadParameter.AcceptChanges();

            // Return the new DataTable. 
            return leadParameter;
        }
    }
}
