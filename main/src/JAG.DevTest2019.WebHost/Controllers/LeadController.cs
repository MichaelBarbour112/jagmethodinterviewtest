﻿using JAG.DevTest2019.Host.Models;
using JAG.DevTest2019.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using System.Timers;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading;
using JAG.DevTest2019.Host.Hubs;

namespace JAG.DevTest2019.Host.Controllers
{
    public class LeadController : Controller
    {
        private static System.Timers.Timer leadTimer;

        public ActionResult Index()
        {
            return View(new LeadViewModel());
        }

        [HttpPost]
        public JsonResult SubmitLead(LeadViewModel model)
        {
            //TODO: 6. Call the WebAPI service here & pass results to UI
            if (model != null && ModelState.IsValid)
            {
                LeadViewModel result = new LeadViewModel()
                {
                    Results = new LeadResultViewModel()
                    {
                        IsSuccessful = true,
                        Message = "Thank you for submitting your details"
                    }
                };

                Lead lead = new Lead()
                {
                    ClientIpAddress = GetIPAddress(),
                    ReferrerUrl = GetRefererURL(),
                    UserAgent = GetUserAgent(),
                    TrackingCode = model.TrackingCode,
                    FirstName = model.FirstName,
                    Surname = model.Surname,
                    EmailAddress = model.EmailAddress,
                    ContactNumber = model.ContactNumber,
                    LeadParameters = new LeadParameter[]
                    {
                        new LeadParameter()
                        {
                            Key = "BloodType",
                            Value = model.BloodType
                        },
                        new LeadParameter()
                        {
                            Key = "Gender",
                            Value = model.Gender
                        },
                        new LeadParameter()
                        {
                            Key = "Language",
                            Value = model.Language
                        },
                    },
                    UserSessionId = model.UserSessionId,
                };

                var pollResult = new Thread(new ThreadStart(() => GetLeadResult(model.UserSessionId)));
                pollResult.Start();
                var postToApi = PostToService(lead);

                if (postToApi != null && !postToApi.IsSuccessful && postToApi.IsDuplicate)
                {
                    result.Results = new LeadResultViewModel()
                    {
                        IsSuccessful = false,
                        Message = "A qoute request has already been received. A representitive will contact you shortly."
                    };
                }
                else if (postToApi != null && postToApi.IsSuccessful)
                {
                    result.Results = new LeadResultViewModel()
                    {
                        LeadId = postToApi.LeadId,
                        IsSuccessful = true,
                        Message = "Thank you for submitting your details"
                    };

                    return Json(new { resultType = "Success", resultMessage = result.Results.Message });
                }
                else
                {
                    return Json(new { resultType = "Error", resultMessage = "Qoutes request failed. Please contact us now on 011 326 3761" });
                }
            }

            return Json(new { resultType = "Error", resultMessage = "Qoutes request failed. Please contact us now on 011 326 3761" });
        }

        private string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        private string GetUserAgent()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            return context.Request.UserAgent;
        }

        private string GetRefererURL()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            return context.Request.UrlReferrer.AbsolutePath;
        }

        private void GetLeadResult(string sessionId)
        {
            var resultFound = false;

            while (!resultFound)
            {
                Thread.Sleep(2000);

                var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        var isSuccessful = false;
                        var isCapped = false;
                        var isDuplicate = false;

                        SqlCommand leadCommand = new SqlCommand("SELECT TOP 1 IsSuccessful, IsCapped, IsDuplicate FROM [Lead] WHERE UserSessionId = @UserSessionId ORDER BY LeadId DESC", connection);
                        leadCommand.Parameters.AddWithValue("@UserSessionId", sessionId);

                        using (SqlDataReader lReader = leadCommand.ExecuteReader())
                        {
                            while (lReader.Read())
                            {
                                isSuccessful = Convert.ToBoolean(lReader["IsSuccessful"]);
                                isCapped = Convert.ToBoolean(lReader["IsCapped"]);
                                isDuplicate = Convert.ToBoolean(lReader["IsDuplicate"]);
                            }
                        }

                        IHubContext _hub = GlobalHost.ConnectionManager.GetHubContext<LeadHub>();
                        var groupKey = string.Format("LeadRequest-{0}", sessionId);
                        var messageToReturn = "";
                        if (isSuccessful)
                        {
                            messageToReturn = "Success";
                        }
                        else if (isCapped)
                        {
                            messageToReturn = "Capped";
                        }
                        else if (isDuplicate)
                        {
                            messageToReturn = "Duplicate";
                        }

                        _hub.Clients.Group(groupKey).updateLeadRequestStatus(messageToReturn);
                        resultFound = true;
                    }
                    catch(Exception ex)
                    {
                        //Log errors to audit log if required
                        Console.WriteLine("An error has occured. Failed to get new lead status.");
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        
        private LeadResponse PostToService(Lead lead)
        {
            string endPoint = System.Configuration.ConfigurationManager.AppSettings["JAGServiceEndpoint"];
            var client = new RestClient(endPoint);

            var request = new RestRequest("api/lead", Method.POST);
            request.AddJsonBody(lead);

            // execute the request
            IRestResponse response = client.Execute(request);
            return JsonConvert.DeserializeObject<LeadResponse>(response.Content);
        }
    }
}