﻿var JAG;
(function (JAG) {
    var Common;
    (function (Common) {
        function toggleButton(sender) {
            var $btn = $(sender);
            if ($btn.attr("data-loading") == "true") {
                $btn.removeAttr("data-loading");
                $btn.removeAttr("disabled");
                var $icon = $("i:hidden", $btn);
                if ($icon.length > 0) {
                    $icon.show();
                }
                $btn.children().first('i').remove();
                $btn.attr("data-loading", "false");
            }
            else {
                $btn.attr("data-loading", "true");
                $("i", $btn).hide();
                $btn.prepend("<i class='fa fa-circle-o-notch fa-spin'></i> ");
                $btn.attr("disabled", "disabled");
            }
        }
        Common.toggleButton = toggleButton;
    })(Common = JAG.Common || (JAG.Common = {}));
})(JAG || (JAG = {}));