﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace JAG.DevTest2019.Host.Hubs
{
    public class LeadHub : Hub
    {
        public async System.Threading.Tasks.Task JoinLeadRequest(string sessionId)
        {
            var groupKey = string.Format("LeadRequest-{0}", sessionId);
            await Groups.Add(Context.ConnectionId, groupKey);
        }
    }
}