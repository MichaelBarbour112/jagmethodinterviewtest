﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[assembly: OwinStartup(typeof(JAG.DevTest2019.Host.App_Start.Startup))]
namespace JAG.DevTest2019.Host.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            #region SignalR
            app.MapSignalR();
            #endregion
        }
    }
}